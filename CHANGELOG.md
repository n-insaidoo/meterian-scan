# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.6

- patch: (1a42c7e) Merged in go-bitbucket-hosted-private-module-update (pull request #3)

Now also supporting private go modules hosted on BitBucket and GitLab

## 1.0.5

- patch: (bf29306) Merged in pipe-update (pull request #2)

Pipe update

## 1.0.4

- patch: Now pointing at the correct docker image in the pipe.yml

## 1.0.3

- patch: Renamed the pipe as part of BitBucket Official pipes submission

## 1.0.2

- patch: Updated the pipe to support the usage of the WORKDIR parameter

## 1.0.1

- patch: Now pointing to correct client path within the container

## 1.0.0

- major: Major update for the Meterian bitbucket pipe

## 0.1.0

- minor: Initial release

