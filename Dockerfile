FROM meterian/cli:latest

# Copy necessary scripts from pipe/
COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
