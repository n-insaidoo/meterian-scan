#!/usr/bin/env bash

set -x
IMAGE=$1
BITBUCKET_CLONE_DIR=${BITBUCKET_CLONE_DIR:=$(pwd)}

##
# Step 1: Generate new version
##
lastCommitSha="$(git log --format="%H" -n 1)"
lastCommitMessage="$(git log -1 --pretty=%B)"

previous_version=$(semversioner current-version)
# Get the release type (e.g. [major|minor|patch])from ci-scripts/release-type.sh
source "${BITBUCKET_CLONE_DIR}/ci-scripts/release-type.sh"
semversioner add-change --type "${RELEASE_TYPE}" --description "(${lastCommitSha:0:7}) ${lastCommitMessage}"
semversioner release
new_version=$(semversioner current-version)

##
# Step 2: Generate CHANGELOG.md
##
echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md
# Use new version in the README.md examples
echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" README.md
# Use new version in the pipe.yml metadata file
echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" pipe.yml

##
# Step 3: Build and push docker image
##
echo "Build and push docker image..."
echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
docker build -t ${IMAGE} .
docker tag ${IMAGE} ${IMAGE}:${new_version}
docker push ${IMAGE}

##
# Step 4: Commit back to the repository
##
echo "Committing updated files to the repository..."
git add .
git commit -m "Update files for new version '${new_version}' [skip ci]"
git push origin ${BITBUCKET_BRANCH}

##
# Step 5: Tag the repository
##
echo "Tagging for release ${new_version}" "${new_version}"
git tag -a -m "Tagging for release ${new_version}" "${new_version}"
git push origin ${new_version}
