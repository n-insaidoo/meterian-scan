#!/bin/bash
#
# Required globals:
#   METERIAN_API_TOKEN
#
# Optional globals:
#   WORKSPACE (default: ".")
#   METERIAN_CLIENT_ARGS (default: "")
#   DEBUG (default: "false")
#

source "/tmp/common.sh"

githubPrivateCustomConfig() {
	echo "machine github.com login "${BP_GITHUB_USER}" password "${BP_GITHUB_TOKEN}"" >> "${HOME}/.netrc"
}

bitbucketPrivateCustomConfig() {
	echo "machine bitbucket.org login "${BP_BITBUCKET_USER}" password "${BP_BITBUCKET_APP_PASSWORD}"" >> "${HOME}/.netrc"
	echo "machine api.bitbucket.org login "${BP_BITBUCKET_USER}" password "${BP_BITBUCKET_APP_PASSWORD}"" >> "${HOME}/.netrc"
}

gitlabPrivateCustomConfig() {
	echo "machine gitlab.com login "${BP_GITLAB_USER}" password "${BP_GITLAB_TOKEN}"" >> "${HOME}/.netrc"
}

privateVCConfigs() {
	if [[ -n "${BP_GITHUB_USER:-}" && -n "${BP_GITHUB_TOKEN}" ]]; then
		githubPrivateCustomConfig
	fi

	if [[ -n "${BP_BITBUCKET_USER:-}" && -n "${BP_BITBUCKET_APP_PASSWORD:-}" ]]; then
		bitbucketPrivateCustomConfig
	fi

	if [[ -n "${BP_GITLAB_USER:-}" && -n "${BP_GITLAB_TOKEN:-}" ]]; then
		gitlabPrivateCustomConfig
	fi
}

goVersionControlCustomConfig() {
	privateVCConfigs
}
goVersionControlCustomConfig

getLastModifiedDateForFile() {
	MAYBE_FILE=$1

	WHEN=`date -r $MAYBE_FILE +"%Y-%m-%d" 2>/dev/null`
	if [[ $? > 0 ]]; then 
		WHEN='1999-01-01'
	fi

	# returning the value of $WHEN ( common way of returning data from functions in bash )
	echo $WHEN
}

updateClient() {
	METERIAN_JAR_PATH=$1
	CLIENT_TARGET_URL=$2

	info "Checking the client..."
	curl -s -o ${METERIAN_JAR_PATH} "${CLIENT_TARGET_URL}"  >/dev/null
}

# Required parameters
METERIAN_API_TOKEN=${METERIAN_API_TOKEN:?'METERIAN_API_TOKEN variable missing.'}
WORKSPACE=${WORKSPACE:="."}
METERIAN_CLIENT_ARGS="${METERIAN_CLIENT_ARGS:=""}"
if [[ "${DEBUG}" == "true" ]];then
	METERIAN_CLIENT_ARGS="${METERIAN_CLIENT_ARGS} --debug"
fi

# meterian jar location
METERIAN_JAR=/tmp/meterian-cli.jar

# update the client if necessary
updateClient "${METERIAN_JAR}" "https://www.meterian.com/downloads/meterian-cli.jar"

cd ${WORKSPACE}
info "Running Meterian scan on ${WORKSPACE}"
java -Duser.home=/tmp  -jar ${METERIAN_JAR} ${METERIAN_CLIENT_ARGS} --interactive=false