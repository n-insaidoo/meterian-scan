#!/bin/bash
#
# Use the Meterian scanner on your project to scan for vulnerabilities using this Bitbucket Pipe in your Pipeline configuration
cp "$(dirname "$0")/common.sh" /tmp
source "/tmp/common.sh"

info "Executing the pipe..."

# To uncomment for internal use
# DEBUG=${DEBUG:="false"}
# enable_debug() {
#   if [[ "${DEBUG}" == "true" ]]; then
#     info "Enabling debug mode."
#     set -x
#   fi
# }
# enable_debug

currDir=$(pwd)
WITH_HUID="-ou $(stat -c '%u' "${currDir}")"
WITH_HGID="-g $(stat -c '%g' "${currDir}") -o"

groupadd ${WITH_HGID} meterian
useradd -g meterian ${WITH_HUID} meterian -d /home/meterian

# creating home dir if it doesn't exist
if [[ ! -d "/home/meterian" ]];
then
    mkdir /home/meterian
fi

# changing home dir group and ownership
chown meterian:meterian /home/meterian

# launch meterian client 
run su meterian -c -m /meterian.sh 2> /dev/null

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error! Breaking build."
  exit "${status}"
fi