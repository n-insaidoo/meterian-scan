#!/usr/bin/env bats

BITBUCKET_CLONE_DIR=${BITBUCKET_CLONE_DIR:=$(pwd)}

# Sample projects setup
JAVA_SAMPLE_PERFECT="java-sample-project"
SWIFT_SAMPLE_PERFECT="swift-sample-project"
GO_SAMPLE_PERFECT="go-sample-project"
DOTNET_SAMPLE_PERFECT="dotnet-sample-project"
PHP_SAMPLE_PERFECT="php-sample-project"
RUBY_SAMPLE_PERFECT="ruby-sample-project"
PYTHON_SAMPLE_PERFECT="python-sample-project"
NODE_SAMPLE_PERFECT="node-sample-project"
SCALA_SAMPLE_PERFECT="scala-sample-project"


# Secrets setup
BP_GITHUB_USER="${BP_GITHUB_USER:-}"
BP_GITHUB_TOKEN="${BP_GITHUB_TOKEN:-}"

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/bitbucket-meterian-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .

  # Sample projects setup 
  SAMPLE_PROJECTS_DIR="${BITBUCKET_CLONE_DIR}/workspace"
  echo "Clearing the contents ${REPO_FOLDER}..."
  rm -rf ${SAMPLE_PROJECTS_DIR} || true
  mkdir -p ${SAMPLE_PROJECTS_DIR}
}

teardown() {
  cd ${BITBUCKET_CLONE_DIR}
}

runMeterianScan() {
   PROJECT_DIR="${1}"
   docker run \
        -e METERIAN_API_TOKEN="${METERIAN_API_TOKEN}" \
        -e WORKSPACE="${PROJECT_DIR}" \
        -e BP_GITHUB_USER="${BP_GITHUB_USER}" \
        -e BP_GITHUB_TOKEN="${BP_GITHUB_TOKEN}" \
        -e BP_BITBUCKET_USER="${BP_BITBUCKET_USER}" \
        -e BP_BITBUCKET_APP_PASSWORD="${BP_BITBUCKET_APP_PASSWORD}" \
        -e BP_GITLAB_USER="${BP_GITLAB_USER}" \
        -e BP_GITLAB_TOKEN="${BP_GITLAB_TOKEN}" \
        -e GOPRIVATE="${GOPRIVATE}" \
        -e DEBUG="true"
        -v ${BITBUCKET_CLONE_DIR}:${BITBUCKET_CLONE_DIR} \
        -w ${BITBUCKET_CLONE_DIR} \
        ${DOCKER_IMAGE}:test 
}

@test "Succesfully scan a perfect Java sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${JAVA_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${JAVA_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Successfully scan a perfect Swift sample project" {
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${SWIFT_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${SWIFT_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Successfully scan a perfect Go sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${GO_SAMPLE_PERFECT}"
    #git clone --depth=1 --single-branch --branch "with-private-dependency" "https://github.com/MeterianHQ/${GO_SAMPLE_PERFECT}.git"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${GO_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Successfully scan a perfect Dotnet sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${DOTNET_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${DOTNET_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Succesfully scan a perfect Php sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${PHP_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${PHP_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Succesfully scan a perfect Ruby sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${RUBY_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${RUBY_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Succesfully scan a perfect Python sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${PYTHON_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${PYTHON_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Succesfully scan a perfect Node.js sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${NODE_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${NODE_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}

@test "Succesfully scan a perfect Scala sample project" {
    skip
    ### Setup
    cd $SAMPLE_PROJECTS_DIR
    PROJECT_DIR="${SAMPLE_PROJECTS_DIR}/${SCALA_SAMPLE_PERFECT}"
    git clone --depth=1 "https://meterian-bot@bitbucket.org/meterian-bot/${SCALA_SAMPLE_PERFECT}.git"

    ### Action
    run runMeterianScan "${PROJECT_DIR}" 

    ### Asserts
    echo "Status: $status"
    echo "Output: $output"
    echo "${output}" >> "${PROJECT_DIR}/execution.log"

    ### Assert that the scan finished with zero exit code
    [ "$status" -eq 0 ]
}